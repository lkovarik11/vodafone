<?php declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use Vodafone\SMSSenderService;

$smsSender = new SMSSenderService();
$sendingResolver = new SendingResolver($smsSender);

echo $sendingResolver->index();