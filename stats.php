<?php declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

$statsResolver = new StatsResolver();

echo $statsResolver->index();