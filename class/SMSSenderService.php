<?php

namespace Vodafone;


class SMSSenderService
{
    public function send(string $message): string
    {
        // Sending SMS action

        return "Send SMS - " . $message;
    }
}