<?php

use Vodafone\Parser;
use Vodafone\MessagesEnum;
use Vodafone\SMSSenderService;

class SendingResolver
{
    private $result = [];
    private $sender;

    public function __construct(SMSSenderService $sender)
    {
        $this->sender = $sender;
    }

    public function index(): string
    {
        $parser = new Parser('backend.csv');
        $rows = $parser->parse();

        foreach ($rows as $row) {
            $date = strtotime($row[2]);
            $messages = [];

            if ($row[1] < 200) {
                $messages[] = $this->sender->send(MessagesEnum::LOW_CREDIT);
            }

            if ($date < time() - (60*60*24*7*5)) {
                $messages[] = $this->sender->send(MessagesEnum::LAST_UPDATE);

            }

            if ($row[1] <= 300 && $date > time() - (60*60*24*7*2)) {
                $messages[] = $this->sender->send(MessagesEnum::CREDIT_UPDATE_LIMIT);
            }

            $this->result[] = [$row[0] => $messages];
        }

        return json_encode($this->result);
    }
}