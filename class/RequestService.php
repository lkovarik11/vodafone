<?php

namespace Vodafone;

class RequestService
{
    const ALLOWED_METHODS = ['POST'];
    const ALLOWED_EXTENSIONS = ['.csv'];

    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function getFile(): string
    {
        return $this->request['file']['tmp_name'];
    }
}