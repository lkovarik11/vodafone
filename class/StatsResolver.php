<?php

use Vodafone\Parser;
use Vodafone\MessagesEnum;

class StatsResolver
{
    private $stats = [];

    public function __construct()
    {
        $this->stats[MessagesEnum::LOW_CREDIT] = 0;
        $this->stats[MessagesEnum::LAST_UPDATE] = 0;
        $this->stats[MessagesEnum::CREDIT_UPDATE_LIMIT] = 0;
    }

    public function index(): string
    {
        $parser = new Parser('backend.csv');
        $rows = $parser->parse();

        foreach ($rows as $row) {
            $date = strtotime($row[2]);

            if ($row[1] < 200) {
                $this->stats[MessagesEnum::LOW_CREDIT]++;
            }

            if ($date < time() - (60*60*24*7*5)) {
                $this->stats[MessagesEnum::LAST_UPDATE]++;

            }

            if ($row[1] <= 300 && $date > time() - (60*60*24*7*2)) {
                $this->stats[MessagesEnum::CREDIT_UPDATE_LIMIT]++;
            }
        }

        return json_encode($this->stats);
    }
}