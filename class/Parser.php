<?php

namespace Vodafone;

class Parser
{
    private $file;

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    public function parse(): array
    {
        $result = [];
        if (($handle = fopen($this->file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle)) !== FALSE) {
                $result[] = $data;
            }
            fclose($handle);
        }

        return $result;
    }
}