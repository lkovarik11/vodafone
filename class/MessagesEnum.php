<?php

namespace Vodafone;

class MessagesEnum
{
    public const LOW_CREDIT = "Credit is lower than 200";
    public const LAST_UPDATE = "Last update is older than 5 months";
    public const CREDIT_UPDATE_LIMIT = "CREDIT <= 300 && LAST TOP UP DATE < 2 months";
}